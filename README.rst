.. TODO: Complete the README descriptions and "about" section.{% if False %}{# Hiding GitHub README #}

Django project template
=======================

This project template creates a Django 1.5 compatible project with
a base set of applications

Features
---------

Installed apps:

* Django 1.5 compatible.
* Postgres
* Pillow (PIL replacement)
* SORL-Thumbnail_
* South_
* bpython_ (disabled)
* django-admin-tools_ (disabled)
* django-compressor_
* django-crispy-forms_
* django-debugtools_
* django-filebrowser-no-grapelli-django1.4_
* django-fluent-dashboard_ (disabled)
* django-google-analytics_

Configured URLs:

* ``/robots.txt``
* ``/sitemap.xml``
* ``/admin/``

Templates:

* ``base.html``
* ``404.html``
* ``500.html``

Features:

* Sensitive settings loaded from environment variables.
* Separate, version-controlled settings for each target environment.
* Auto-activation of project settings.
* Django Debug Toolbar and Django Extensions pre-loaded for local development.
* make targets for common development tasks
* Run unit tests with coverage.py for coverage reports
* WSGI deployment scripts
* CSS and JavaScript paths configured
* HTML5shiv + jQuery Installed
* Meyer-based CSS reset
* Working Compass_ + SASS_ + LiveReload_ setup (can be discarded)
* Bootstrap with Font Awesome

Usage
-----

Create a Django project::

    mkdir my-website.com
    cd my-website.com
    django-admin.py startproject mywebsite . -e py,rst,example,gitignore --template=https://github.com/chrisfranklin/dpt/archive/master.zip

Alternatively, you can place the files in a ``src`` folder too::

    mkdir -p my-website.com/src
    cd my-website.com
    django-admin.py startproject mywebsite src -e py,rst,example,gitignore --template=https://github.com/chrisfranklin/dpt/archive/master.zip

This allows you to create folders like ``docs``, ``web``, ``logs``, ``etc`` at the toplevel.
This setup is recommended.

The remaining instructions - to start the development server - can be found in the generated ``README.rst`` file.

Working with SCSS files
=======================

CSS files are easier to edit using SASS_ and Compass_.

The advantages of SASS are:

* Variables.
* Nesting CSS selectors.
* Default mixins for CSS3 properties and vendor-prefixes.
* Typography features such as ellipses,
* Generating sprites, including ``background-position`` offsets.

.. note::

    This feature is optional. If you don't like to use it, the project already has a ``screen.css`` file so you can just override there but don't.

Using compass
-------------

Install Compass_ using::

    gem install compass bootstrap-sass oily_png

Leave Compass_ running in the terminal::

    compass watch

It automatically compiles the ``*.css`` files for you.


Using guard+livereload
----------------------

To make it even better, use guard-livereload_.
Now the browser can automatically refresh all styles inline.

Install guard-livereload_ using::

    gem install guard-livereload guard-compass

Leave it running in the terminal during development::

    guard

Install a browser plugin, see:

* Firefox (2.0.9 dev release): https://github.com/siasia/livereload-extensions/downloads
* Everyone else: http://help.livereload.com/kb/general-use/browser-extensions

And toggle the "LiveReload" button in the browser at the desired page.

Each time a change is made in ``*.scss`` files, the files are compiled and the browser reloads
the CSS file, even without reloading the entire page!

.. _bpython: http://bpython-interpreter.org/
.. _django-admin-tools: https://bitbucket.org/izi/django-admin-tools
.. _django-compressor: http://django_compressor.readthedocs.org/
.. _django-crispy-forms: http://django-crispy-forms.readthedocs.org/
.. _django-debugtools: https://github.com/edoburu/django-debugtools
.. _django-filebrowser-no-grapelli-django1.4: https://github.com/wardi/django-filebrowser-no-grappelli
.. _django-fluent-dashboard: https://github.com/edoburu/django-fluent-dashboard
.. _django-google-analytics: https://github.com/clintecker/django-google-analytics
.. _LiveReload: http://livereload.com/
.. _SORL-Thumbnail: https://github.com/sorl/sorl-thumbnail
.. _South: http://south.readthedocs.org/


------------

.. {% else %}

{{ project_name|title }} Project
========================================

About
-----

Describe your project here.

Prerequisites
-------------

- Python >= 2.6
- pip
- virtualenv (virtualenvwrapper is recommended)

Installation
------------

To setup a local development environment::

    mkvirtualenv {{ project_name }}
    pip install -r requirements/local.txt # or production, testing etc

-----------------------------
Setting Environment Variables
-----------------------------

Instead of keeping sensitive data like the project ``SECRET_KEY`` and
database connection strings in settings files, or worse, keeping them
in an unversioned ``local_settings`` module, we use environment
variables to store these bits of data.

If you're using virtualenvwrapper, a convenient place to define these
is in your ``postactivate`` script. Otherwise, they can go in e.g.
``~/bash_profile``.

You can use a tool like `this secret key generator`_ to generate
a ``SECRET_KEY``.

.. _this secret key generator: http://www.miniwebtool.com/django-secret-key-generator/

**NOTE:** Be sure to keep a backup copy of the ``SECRET_KEY`` used in production!!

Here is a list of the required environment variables:

* {{ project_name|upper }}_DATABASE_NAME

* {{ project_name|upper }}_DATABASE_USER

* {{ project_name|upper }}_DATABASE_PASSWORD

* {{ project_name|upper }}_SECRET_KEY

* {{ project_name|upper }}_DJRILL_WEBHOOK_SECRET

* {{ project_name|upper }}_MANDRILL_API_KEY

The correct way of doing this is to use Autoenv from https://github.com/kennethreitz/autoenv ::

    cp settings.env.rst settings.env
    nano settings.env

Then configure the neccesary settings, whenever you change directory into the project directory this
script will be run, you can add any lines you want to it for project specific settings but the file
is not included in the repository so each user must maintain their own file based on the example.

If you are using virtualenvwrapper, you could alternatively use a ``postactivate`` script as follows::

    cdvirtualenv
    vim bin/postactivate
    
Set the contents as follows, note that you should only use this method if you know what you are doing::

    #!/bin/bash
    # This hook is run after this virtualenv is activated.
    
    export {{ project_name|upper }}_DATABASE_NAME="{{ project_name}}";
    export {{ project_name|upper }}_DATABASE_USER="postgres";
    export {{ project_name|upper }}_DATABASE_PASSWORD="";
    export {{ project_name|upper }}_SECRET_KEY="PLEASeS33tMe!!#";
    export {{ project_name|upper }}_DJRILL_WEBHOOK_SECRET="SETME!!";
    export {{ project_name|upper }}_MANDRILL_API_KEY="SetMe!"; 
    export DJANGO_SETTINGS_MODULE="project.settings.local";

The following code will help::

    cdvirtualenv; nano bin/postactivate
    popd

The last line, which sets ``DJANGO_SETTINGS_MODULE`` to ``project.settings.local``,
is not strictly necessary, but helpful to avoid the need for the
``--settings`` flag to django management commands.


Running ``manage.py`` commands
------------------------------

Django's ``manage.py`` script is located in the ``apps`` directory. Any
``manage.py`` command can be run as follows::

    python apps/manage.py --settings=project.settings.local COMMAND


**NOTE:** If you've set the ``DJANGO_SETTINGS_MODULE`` environment variable,
you can omit the ``--settings=...`` portion of any ``manage.py`` commands.

For convenience, {{ project_name|capfirst }} provides makefile targets for most
common ``manage.py`` commands. 


Create your Postgres Database
-----------------------------

You must create a Postgres Database to use this project, please run the following::

    sudo su - postgres
    psql
    create user {{ project_name}} with password 'password';
    create database {{ project_name}};
    grant all privileges on database {{ project_name}} to {{ project_name}};


Initialize Your Database
------------------------

{{ project_name|capfirst }} uses South_ to manage database migrations.

::

    make db


Start the Development Server
----------------------------

::

    make server

Now `bask in the glory`_ of all the hard work you didn't have to do to get this far!

.. _bask in the glory: http://localhost:8000/


Compiling CSS files
-------------------

To compile SASS_ files::

    gem install compass bootstrap-sass oily_png guard-livereload guard-compass

    guard

To enable LiveReload_ of ``*.css`` files during development, install a browser plugin:

* Firefox (2.0.9 dev release): https://github.com/siasia/livereload-extensions/downloads
* Everyone else: http://help.livereload.com/kb/general-use/browser-extensions

And toggle the "LiveReload" button in the browser at the desired page.


Running Tests
-------------

To run project tests and generate a coverage report, run::

    make test

Open ``htmlcov/index.html`` in your browser to view the coverage report.


Deploying
---------

There is an experimental ``fabfile`` included, which will need to be edited
to fit your needs. Change this documentation as required.


License
-------

Describe project license here.


.. Add links here:{% endif %}

.. _Compass: http://compass-style.org/
.. _LiveReload: http://livereload.com/
.. _guard-livereload: https://github.com/guard/guard-livereload
.. _SASS: http://sass-lang.com/
