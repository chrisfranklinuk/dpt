# Base requirements file.
# Place items here that should be installed in ALL environments.

# Django
# Add your django version here

## Needed by the settings base module. Do not remove.
Unipath==0.2.1
South==0.7.6

# For setting up environment variables. Do not remove.
# https://github.com/kennethreitz/autoenv
autoenv
# Cacheing
python-memcached >= 1.48, < 2.0
django_compressor
sorl-thumbnail==11.12
django-google-analytics
-e git+git://github.com/smacker/django-filebrowser-no-grappelli-django14.git#egg=django-filebrowser
#####################################################
#
# Below are some commonly used django apps, included
# here for convenience.
# Be sure to add version numbers here!!!!
#
#####################################################

# Convenient utilities for Django Models
# https://github.com/carljm/django-model-utils
# django-model-utils

# PostgreSQL Database Adapter
psycopg2

# The missing pieces to Django CBV
# http://django-braces.readthedocs.org/en/latest/index.html
# django-braces

# Programmatically define form layouts
# http://django-crispy-forms.readthedocs.org/en/latest/
django-crispy-forms

# Adds HTML 5 form attributes to Django forms
# http://django-floppyforms.readthedocs.org/en/latest/ 
django-floppyforms

# Bootstrap django admin
# https://github.com/riccardo-forina/django-admin-bootstrapped
# django-admin-bootstrapped==0.3.2

# Djrill integrates the Mandrill transactional email service into Django.
# https://github.com/brack3t/Djrill
djrill

# This is a simple API wrapper for Mandrill's inbound email webhook in Python
# https://github.com/jpadilla/mandrill-inbound-python
python-mandrill-inbound

# Update django url patterns to django 1.5
# https://github.com/futurecolors/django-future-url
django-future-url

# Simple account management with Bootstrap
# https://github.com/pinax/django-user-accounts
django-user-accounts
#######################################################
#
# Below are the project specific applications
#
#######################################################
