# This hook is run when you open the project directory.
export {{ project_name|upper }}_DATABASE_NAME="{{ project_name}}";
export {{ project_name|upper }}_DATABASE_USER="postgres";
# You should change the values below
export {{ project_name|upper }}_DATABASE_PASSWORD="";
export {{ project_name|upper }}_SECRET_KEY="PLEASeS33tMe!!#";
export {{ project_name|upper }}_DJRILL_WEBHOOK_SECRET="SETME!!";
export {{ project_name|upper }}_MANDRILL_API_KEY="SetMe!";
# Not neccesary but avoids having to specify --settings. 
export DJANGO_SETTINGS_MODULE="project.settings.local";
